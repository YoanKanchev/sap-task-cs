package com.example.ComputerScience.controllers.rest;

import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.services.DisciplineService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/disciplines")
public class DisciplineController {
    private final DisciplineService disciplineService;

    @GetMapping()
    public ResponseEntity<List<Discipline>> getAll() {
        return ResponseEntity.ok().body(disciplineService.getTopDisciplines());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Discipline> getById(@PathVariable int id) {
        return ResponseEntity.ok().body(disciplineService.getById(id));
    }

//    @GetMapping("/")
//    public List<Discipline> filter(DisciplineFilterDto appServiceFilterDto) {
//        return disciplineService.filter(new DisciplineParameterDto(appServiceFilterDto.getType(),
//                appServiceFilterDto.getCreatorName(), appServiceFilterDto.getRating(), appServiceFilterDto.getSortBy(),
//                appServiceFilterDto.isOrderAsc()));
//    }
}
