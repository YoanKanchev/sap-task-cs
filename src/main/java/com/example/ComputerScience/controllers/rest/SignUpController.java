package com.example.ComputerScience.controllers.rest;


import com.example.ComputerScience.controllers.error.ErrorController;
import com.example.ComputerScience.models.dto.ChangePasswordDto;
import com.example.ComputerScience.models.dto.create.CreateStudentDto;
import com.example.ComputerScience.models.dto.create.CreateTeacherDto;
import com.example.ComputerScience.models.dto.create.CreateUserDto;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;
import com.example.ComputerScience.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

import static com.example.ComputerScience.helpers.GlobalConstants.YOUR_LINK_HAS_EXPIRED;

@Controller
@RequestMapping("/sign-up")
@RequiredArgsConstructor
public class SignUpController {

    private final UserService<User> userService;

    private final UserMaintenanceService<User> userMaintenanceService;
    private final TeacherService teacherService;
    private final StudentService studentService;
    private final TokenService tokenService;
    private final ErrorController errorController;
    private final EmailSenderService emailSenderService;


    @PostMapping("/user")
    public ResponseEntity<Object> createUser(@Validated @ModelAttribute CreateUserDto userDto, BindingResult bindingResult) {

        return bindingResult.hasErrors() ?
                errorController.handleBindingErrors(bindingResult) :
                ResponseEntity.ok(userService.create(userDto));
    }

    @PostMapping("/teacher")
    public ResponseEntity<Object> createTeacher(@Validated @ModelAttribute CreateTeacherDto teacherDto, BindingResult bindingResult) {

        return bindingResult.hasErrors() ?
                errorController.handleBindingErrors(bindingResult) :
                ResponseEntity.ok(teacherService.create(teacherDto));
    }

    @PostMapping("/student")
    public ResponseEntity<Object> createStudent(@Validated @ModelAttribute CreateStudentDto studentDto, BindingResult bindingResult) {

        return bindingResult.hasErrors() ?
                errorController.handleBindingErrors(bindingResult) :
                ResponseEntity.ok(studentService.create(studentDto));
    }

    @PostMapping("/generate-token")
    public ResponseEntity<UserToken> generateToken(@RequestParam String email, @RequestParam String tokenType) {
        User user = userService.getByEmail(email);
        return ResponseEntity.ok(tokenService.newToken(user, tokenType));
    }

    @PostMapping("/activate-user")
    public ResponseEntity<Object> verifyUser(@RequestParam String token) {
        UserToken userToken = tokenService.searchByName(token);

        if (userToken.isExpired()) {
            return new ResponseEntity<>(YOUR_LINK_HAS_EXPIRED, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(userMaintenanceService.activateWithToken(userToken));
    }

    @PostMapping("/recover-password")
    public ResponseEntity<Object> recoverPasswordHandle(@Valid @ModelAttribute ChangePasswordDto passwordDto, @RequestParam String token) {
        UserToken userToken = tokenService.searchByName(token);

        if (userToken.isExpired()) {
            return new ResponseEntity<>(YOUR_LINK_HAS_EXPIRED, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(userMaintenanceService.resetPasswordWithToken(passwordDto, userToken));
    }

}
