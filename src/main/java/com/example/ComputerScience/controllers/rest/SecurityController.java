package com.example.ComputerScience.controllers.rest;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.ComputerScience.controllers.error.ErrorController;
import com.example.ComputerScience.services.security.JWTService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.example.ComputerScience.helpers.GlobalConstants.JWT_TOKEN_PREFIX;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/security")
public class SecurityController {
    private final JWTService jwtService;
    private final ErrorController errorController;

    @GetMapping("/refreshToken")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorization = request.getHeader(AUTHORIZATION);

        if (Strings.isNotEmpty(authorization) && authorization.startsWith(JWT_TOKEN_PREFIX)) {
            try {
                String refreshToken = authorization.replace(JWT_TOKEN_PREFIX, "");
                String newAccessToken = jwtService.refreshAccessToken(request.getRequestURL().toString(), refreshToken);
                Map<String, String> tokens = Map.of("accessToken", newAccessToken, "refreshToken", refreshToken);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (TokenExpiredException e) {
                errorController.handleTokenExpiredConflict(e, response);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
    }
}
