package com.example.ComputerScience.controllers.rest;

import com.example.ComputerScience.models.impl.Teacher;
import com.example.ComputerScience.services.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/teachers")
public class TeacherController {
    private final TeacherService teacherService;

    @GetMapping()
    public ResponseEntity<List<Teacher>> getAll() {
        return ResponseEntity.ok().body(teacherService.getAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getById(@PathVariable int id) {
        return ResponseEntity.ok().body(teacherService.getById(id));
    }

//    @GetMapping("/")
//    public List<Teacher> filter(TeacherFilterDto appServiceFilterDto) {
//        return teacherService.filter(new TeacherParameterDto(appServiceFilterDto.getType(),
//                appServiceFilterDto.getCreatorName(), appServiceFilterDto.getRating(), appServiceFilterDto.getSortBy(),
//                appServiceFilterDto.isOrderAsc()));
//    }
}
