package com.example.ComputerScience.controllers.rest;

import com.example.ComputerScience.models.impl.Student;
import com.example.ComputerScience.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/students")
public class StudentController {
    private final StudentService studentService;

    @GetMapping()
    public ResponseEntity<List<Student>> getAll() {
        return ResponseEntity.ok().body(studentService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Student> getById(@PathVariable int id) {
        return ResponseEntity.ok().body(studentService.getById(id));
    }

    @PostMapping("/{id}/assignDiscipline")
    public ResponseEntity<Student> addDisciplineToStudent(@PathVariable int id, @RequestParam int disciplineId) {
        return ResponseEntity.ok().body(studentService.addDisciplineToStudent(id, disciplineId));
    }
    @PostMapping("/{id}/removeDiscipline")
    public ResponseEntity<Student> removeDisciplineToStudent(@PathVariable int id, @RequestParam int disciplineId) {
        return ResponseEntity.ok().body(studentService.addDisciplineToStudent(id, disciplineId));
    }

    @PostMapping("/assignDiscipline")
    public ResponseEntity<Student> addDisciplineToCurrentStudent(@RequestParam int disciplineId, Principal principal) {
        Student student = studentService.getByUsername(principal.getName());
        return ResponseEntity.ok().body(studentService.removeDisciplineFromStudent(student.getId(), disciplineId));
    }
    @PostMapping("/removeDiscipline")
    public ResponseEntity<Student> removeDisciplineToCurrentStudent(@RequestParam int disciplineId, Principal principal) {
        Student student = studentService.getByUsername(principal.getName());
        return ResponseEntity.ok().body(studentService.removeDisciplineFromStudent(student.getId(), disciplineId));
    }
//    @GetMapping("/")
//    public List<Student> filter(StudentFilterDto appServiceFilterDto) {
//        return studentService.filter(new StudentParameterDto(appServiceFilterDto.getType(),
//                appServiceFilterDto.getCreatorName(), appServiceFilterDto.getRating(), appServiceFilterDto.getSortBy(),
//                appServiceFilterDto.isOrderAsc()));
//    }
}
