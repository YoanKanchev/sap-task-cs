package com.example.ComputerScience.controllers.error;


import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.ComputerScience.exceptions.BadRequestException;
import com.example.ComputerScience.exceptions.DuplicateEntityException;
import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.StringJoiner;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestControllerAdvice
public class ErrorController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleConflict(EntityNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = BadRequestException.class)
    protected ResponseEntity<Object> handleConflict(BadRequestException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    protected ResponseEntity<Object> handleConflict(DuplicateEntityException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = MessagingException.class)
    protected ResponseEntity<Object> handleConflict(MessagingException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    public void handleTokenExpiredConflict(TokenExpiredException ex, HttpServletResponse response) throws IOException {
        response.setHeader("error", ex.getMessage());
        response.setStatus(FORBIDDEN.value());
        Map<String, String> tokens = Map.of("error_message", ex.getMessage());
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), tokens);
    }

    public ResponseEntity<Object> handleBindingErrors(BindingResult bindingResult){
        return new ResponseEntity<>(bindingResult.getAllErrors().get(0).getDefaultMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    public String getErrorsFromBindingResult(BindingResult bindingResult) {
        StringJoiner output = new StringJoiner(", \n");
        bindingResult.getAllErrors()
                .forEach(objectError -> output.add(objectError.getDefaultMessage()));
        return output.toString();
    }
}
