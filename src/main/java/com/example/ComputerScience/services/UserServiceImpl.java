package com.example.ComputerScience.services;

import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.models.dto.ChangePasswordDto;
import com.example.ComputerScience.models.dto.Dto;
import com.example.ComputerScience.models.dto.create.CreateUserDto;
import com.example.ComputerScience.models.dto.update.UpdateUserDto;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;
import com.example.ComputerScience.repositories.UserRepository;
import com.example.ComputerScience.services.mappers.ModelMapperService;
import com.example.ComputerScience.services.validation.UserValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.ComputerScience.helpers.GlobalConstants.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService<User>, UserDetailsService, UserMaintenanceService<User> {
    private final UserRepository<User> userRepository;
    private final ModelMapperService mapperService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserValidationService<User> userValidationService;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER + WITH_ID_NOT_FOUND_ERROR, id)));
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER + WITH_USERNAME_NOT_FOUND_ERROR, username)));
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format(USER + WITH_EMAIL_NOT_FOUND_ERROR, email)));
    }

    @Override
    public User activateWithToken(UserToken token) {
        User user = token.getUser();
        userValidationService.validateToken(user, token);
        user.setActive(true);
        return userRepository.save(user);
    }

    @Override
    public User create(Dto data) {
        CreateUserDto userData = (CreateUserDto) data;
        userData.setPassword(passwordEncoder.encode(userData.getPassword()));
        User user = mapperService.toUser(userData);
        return userRepository.save(user);
    }

    @Override
    public User update(Dto data) {
        UpdateUserDto userData = (UpdateUserDto) data;

        return null;
    }

    @Override
    public User resetPasswordWithToken(ChangePasswordDto updateData, UserToken token) {
        User user = token.getUser();
        String newPassword = passwordEncoder.encode(updateData.getNewPassword());
        user.setPassword(newPassword);
        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getRoles());
    }
}
