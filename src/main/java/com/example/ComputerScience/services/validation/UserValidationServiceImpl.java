package com.example.ComputerScience.services.validation;

import com.example.ComputerScience.exceptions.AuthorisationException;
import com.example.ComputerScience.exceptions.DuplicateEntityException;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;
import com.example.ComputerScience.repositories.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

import static com.example.ComputerScience.helpers.GlobalConstants.*;
import static com.example.ComputerScience.helpers.enums.Role.ROLE_ADMIN;


@Component
public class UserValidationServiceImpl<T extends User> implements UserValidationService<T> {

    private final UserRepository<T> userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserValidationServiceImpl(UserRepository<T> userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void roleAuthorisationValidation(T loggedUser, String role) {
        if ((!loggedUser.isActive())
                || !loggedUser.hasRole(role)) {
            throw new AuthorisationException(UNAUTHORISED_ERROR);
        }
    }

    @Override
    public void validateUniqueUserName(String name) {
        if (userRepository.existsByUsername(name)) {
            throw new DuplicateEntityException(
                    String.format(USER + WITH_NAME_EXISTS_ERROR, name));
        }
    }

    @Override
    public void validateUniqueEmail(String email) {
        if (userRepository.existsByEmail(email)) {
            throw new DuplicateEntityException(
                    String.format(USER + WITH_EMAIL_EXISTS_ERROR, email));
        }
    }

    @Override
    public void validateUniqueUserName(String username, Integer userId) {
        Optional<T> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isPresent()) {
            T userToCheck = optionalUser.get();
            if (!Objects.equals(userToCheck.getId(), userId)) {
                throw new DuplicateEntityException(
                        String.format(USER + WITH_NAME_EXISTS_ERROR, username));
            }
        }
    }

    @Override
    public void validateUniqueEmail(String email, Integer userId) {
        Optional<T> optionalUser = userRepository.findByEmail(email);
        if (optionalUser.isPresent()) {
            T userToCheck = optionalUser.get();
            if (!Objects.equals(userToCheck.getId(), userId)) {
                throw new DuplicateEntityException(
                        String.format(USER + WITH_EMAIL_EXISTS_ERROR, email));
            }
        }
    }

    @Override
    public void validateCorrectPassword(String password, String realPassword) {
        boolean correctPassword = !Strings.isEmpty(password) && passwordEncoder.matches(password, realPassword);
        if (!correctPassword)
            throw new AuthorisationException(WRONG_CONFIRMATION_PASSWORD);
    }

    @Override
    public void validateAdmin(T loggedUser) {
        boolean loggedUserIsAdmin = loggedUser.hasRole(ROLE_ADMIN.toString());
        if (!loggedUserIsAdmin)
            throw new AuthorisationException(UNAUTHORISED_ERROR);
    }

    @Override
    public void validateToken(T user, UserToken token) {
        boolean isCreator = token.getUser().equals(user);
        if (!isCreator)
            throw new AuthorisationException(UNAUTHORISED_ERROR);
    }

}
