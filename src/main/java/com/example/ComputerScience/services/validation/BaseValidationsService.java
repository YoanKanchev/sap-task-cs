package com.example.ComputerScience.services.validation;

public interface BaseValidationsService<T> {

    void adminCheck(T loggedUser);

    <J extends Enum<J>> void validateEnum(String toValidate, Class<J> enumerator, String errorMessage);

}
