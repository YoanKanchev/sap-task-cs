package com.example.ComputerScience.services.validation;

import com.example.ComputerScience.exceptions.AuthorisationException;
import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.models.impl.User;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static com.example.ComputerScience.helpers.GlobalConstants.UNAUTHORISED_ERROR;
import static com.example.ComputerScience.helpers.enums.Role.ROLE_ADMIN;


@Service
public class BaseValidationsServiceImpl<T extends User> implements BaseValidationsService<T> {

		@Override
		public void adminCheck(T loggedUser) {
				if ((!loggedUser.isActive())
						|| (!loggedUser.hasRole(ROLE_ADMIN.toString()))) {
						throw new AuthorisationException(UNAUTHORISED_ERROR);
				}
		}

	@Override
	public <J extends Enum<J>> void validateEnum(String toValidate, Class<J> enumerator, String errorMessage) {
		Arrays.stream(enumerator.getEnumConstants()).filter(x -> x.toString().equals(toValidate)).findAny()
				.orElseThrow(() -> new EntityNotFoundException(errorMessage));
	}
}
