package com.example.ComputerScience.services.validation;


import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;

public interface UserValidationService<T extends User> {

		void roleAuthorisationValidation(T loggedUser, String role);

		void validateUniqueUserName(String name);

		void validateUniqueEmail(String email);

		void validateUniqueUserName(String username, Integer userId);

		void validateUniqueEmail(String email, Integer userId);

		void validateCorrectPassword(String password, String realPassword);

		void validateAdmin(T loggedUser);

		void validateToken(T user, UserToken token);
}
