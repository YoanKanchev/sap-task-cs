package com.example.ComputerScience.services;

import com.example.ComputerScience.models.dto.ChangePasswordDto;
import com.example.ComputerScience.models.impl.UserToken;
import org.springframework.stereotype.Service;

@Service
public interface UserMaintenanceService<T> {
    T activateWithToken(UserToken token);

    T resetPasswordWithToken(ChangePasswordDto updateData, UserToken token);
}
