package com.example.ComputerScience.services;


import com.example.ComputerScience.models.dto.EmailDtoHtml;
import com.example.ComputerScience.models.dto.EmailDtoText;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

import static com.example.ComputerScience.helpers.GlobalConstants.*;


@PropertySource("classpath:application.properties")
@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    private final JavaMailSender mailSender;
    private final String from;
    private final SpringTemplateEngine thymeleafTemplateEngine;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender mailSender, Environment env, SpringTemplateEngine thymeleafTemplateEngine) {
        this.mailSender = mailSender;
        this.from = env.getProperty("spring.mail.username");
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
    }

    @Override
    @Async
    public void sendEmail(EmailDtoText emailDtoText) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(emailDtoText.to());
        email.setSubject(emailDtoText.subject());
        email.setFrom(from);
        email.setText(emailDtoText.message());
        mailSender.send(email);
    }

    @Override
    public void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(emailDtoHtml.templateModel());
        String htmlBody = thymeleafTemplateEngine.process(emailDtoHtml.template(), thymeleafContext);
        sendHtmlMessage(emailDtoHtml.to(), emailDtoHtml.subject(), htmlBody);
    }

    @Override
    public void sendRegistrationConfirmationEmail(UserToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_REGISTRATION_CONFIRMATION + token.getValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_REGISTRATION_CONFIRMATION,
                TEMPLATE_FOR_REGISTRATION_CONFIRMATION);
        sendHtmlEmailFromTemplate(emailDto);
    }

    @Override
    public void sendPasswordRecoveryMail(UserToken token, String url) throws MessagingException {
        String fullUrl = url + PATH_FOR_PASSWORD_RECOVERY_HANDLER + token.getValue();
        EmailDtoHtml emailDto = generateEmailDtoHtml(token, fullUrl, SUBJECT_FOR_PASSWORD_RECOVERY, TEMPLATE_FOR_PASSWORD_RECOVERY);
        sendHtmlEmailFromTemplate(emailDto);
    }

    private void sendHtmlMessage(String to, String subject, String htmlBody) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        mailSender.send(message);
    }

    private EmailDtoHtml generateEmailDtoHtml(UserToken token, String url, String subject, String template) {
        User user = token.getUser();
        String username = user.getFirstName() + " " + user.getLastName();
        String userEmail = user.getEmail();

        Map<String, Object> htmlParameters = Map.of("username", username, "url", url);

        return new EmailDtoHtml(userEmail, subject, template, htmlParameters);
    }

}
