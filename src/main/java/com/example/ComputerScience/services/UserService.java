package com.example.ComputerScience.services;

import com.example.ComputerScience.models.dto.Dto;

import java.util.List;

public interface UserService<T> {
    List<T> getAll();

    T getById(int id);

    T getByUsername(String username);

    T getByEmail(String email);

    T create(Dto data);

    T update(Dto data);
}
