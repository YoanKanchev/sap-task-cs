package com.example.ComputerScience.services;

import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.models.dto.create.CreateDisciplineDto;
import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.repositories.DisciplineRepository;
import com.example.ComputerScience.services.mappers.ModelMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.ComputerScience.helpers.GlobalConstants.*;

@Service
@RequiredArgsConstructor
public class DisciplineServiceImpl implements DisciplineService {

    private final DisciplineRepository disciplineRepository;
    private final ModelMapperService mapperService;

    @Override
    public List<Discipline> getAll() {
        return disciplineRepository.findAll();
    }

    @Override
    public Discipline getById(int id) {
        return disciplineRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(DISCIPLINE + WITH_ID_NOT_FOUND_ERROR, id)));
    }

    @Override
    public List<Discipline> getTopDisciplines() {
        return disciplineRepository.getTopDisciplines(Pageable.ofSize(MAX_SIZE_FOR_TOP_DISCIPLINES));
    }

    @Override
    public List<Discipline> getByCourse(int course) {
        return disciplineRepository.findAllByCourse(course);
    }

    @Override
    public Discipline create(CreateDisciplineDto disciplineData, User taughtBy) {
        Discipline discipline = mapperService.toDiscipline(disciplineData, taughtBy);
        return disciplineRepository.save(discipline);
    }
}
