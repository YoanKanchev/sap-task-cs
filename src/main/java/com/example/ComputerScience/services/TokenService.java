package com.example.ComputerScience.services;


import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;

import java.util.List;

public interface TokenService {

    List<UserToken> getAll();

    UserToken searchByName(String tokenValue);

    UserToken searchById(int id);

    void delete(int id, User loggedUser);

    UserToken newToken(User user, String tokenType);
}
