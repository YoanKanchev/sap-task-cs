package com.example.ComputerScience.services;


import com.example.ComputerScience.models.dto.create.CreateDisciplineDto;
import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.models.impl.User;

import java.util.List;

public interface DisciplineService {
    List<Discipline> getAll();

    Discipline getById(int id);

    List<Discipline> getTopDisciplines();

    List<Discipline> getByCourse(int course);

    Discipline create(CreateDisciplineDto disciplineData, User taughtBy);
}
