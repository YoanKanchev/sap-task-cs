package com.example.ComputerScience.services;

import com.example.ComputerScience.models.impl.Student;

import java.util.List;

public interface StudentService extends UserService<Student> {

    List<Student> getTopStudents();

    Student addDisciplineToStudent(int studentId, int disciplineId);

    Student removeDisciplineFromStudent(int studentId, int disciplineId);
}
