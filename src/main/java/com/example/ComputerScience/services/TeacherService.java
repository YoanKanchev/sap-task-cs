package com.example.ComputerScience.services;

import com.example.ComputerScience.models.impl.Teacher;

import java.util.List;

public interface TeacherService extends UserService<Teacher> {
    List<Teacher> getTopTeachers();
}
