package com.example.ComputerScience.services;

import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.helpers.enums.TokenType;
import com.example.ComputerScience.models.impl.User;
import com.example.ComputerScience.models.impl.UserToken;
import com.example.ComputerScience.repositories.TokenRepository;
import com.example.ComputerScience.services.validation.BaseValidationsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.example.ComputerScience.helpers.GlobalConstants.NOT_FOUND_ERROR;
import static com.example.ComputerScience.helpers.GlobalConstants.TOKEN_ENTITY;


@Service
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    public static final int EXPIRATION_TIME_IN_HOURS = 1;
    private final TokenRepository tokenRepository;
    private final BaseValidationsService<?> baseValidationsService;

    @Override
    public List<UserToken> getAll() {
        return tokenRepository.findAll();
    }

    @Override
    public UserToken searchByName(String tokenValue) {
        return tokenRepository.findByValue(tokenValue)
                .orElseThrow(() -> new EntityNotFoundException(TOKEN_ENTITY + NOT_FOUND_ERROR));
    }

    @Override
    public UserToken searchById(int id) {
        return tokenRepository.getById(id);
    }

    @Override
    public void delete(int id, User loggedUser) {
        UserToken token = searchById(id);
        tokenRepository.delete(token);
    }

    @Override
    public UserToken newToken(User user, String tokenType) {
        baseValidationsService.validateEnum(tokenType, TokenType.class, "Unacceptable token type!");
        TokenType type = TokenType.valueOf(tokenType);
        UserToken token = new UserToken(generateTokenValue(), type, user, EXPIRATION_TIME_IN_HOURS);
        return tokenRepository.save(token);
    }

    private String generateTokenValue() {
        String token = UUID.randomUUID().toString();
        while (tokenRepository.findByValue(token).isPresent()) {
            token = UUID.randomUUID().toString();
        }
        return token;
    }
}
