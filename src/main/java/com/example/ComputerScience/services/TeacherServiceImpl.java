package com.example.ComputerScience.services;


import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.models.dto.Dto;
import com.example.ComputerScience.models.dto.create.CreateTeacherDto;
import com.example.ComputerScience.models.impl.Teacher;
import com.example.ComputerScience.repositories.TeacherRepository;
import com.example.ComputerScience.services.mappers.ModelMapperService;
import com.example.ComputerScience.services.validation.UserValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.ComputerScience.helpers.GlobalConstants.*;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final UserValidationService<Teacher> userValidationService;
    private final ModelMapperService mapperService;

    @Override
    public List<Teacher> getAll() {
        return teacherRepository.findAll(Sort.by(USERNAME).ascending());
    }

    @Override
    public Teacher getById(int id) {
        return teacherRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(TEACHER + WITH_ID_NOT_FOUND_ERROR, id)));
    }

    @Override
    public Teacher getByUsername(String username) {
        return teacherRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format(TEACHER + WITH_USERNAME_NOT_FOUND_ERROR, username)));
    }

    @Override
    public Teacher getByEmail(String email) {
        return teacherRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format(TEACHER + WITH_EMAIL_NOT_FOUND_ERROR, email)));
    }


    @Override
    public List<Teacher> getTopTeachers() {
        return teacherRepository.getTopTeachers(Pageable.ofSize(MAX_SIZE_FOR_TOP_TEACHERS));
    }

    @Override
    public Teacher create(Dto data) {
        CreateTeacherDto teacherData = (CreateTeacherDto) data;
        Teacher teacher = mapperService.toTeacher(teacherData);
        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher update(Dto data) {
        return null;
    }
}
