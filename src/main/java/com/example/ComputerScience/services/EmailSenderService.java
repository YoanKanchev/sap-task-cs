package com.example.ComputerScience.services;

import com.example.ComputerScience.models.dto.EmailDtoHtml;
import com.example.ComputerScience.models.dto.EmailDtoText;
import com.example.ComputerScience.models.impl.UserToken;
import org.springframework.scheduling.annotation.Async;

import javax.mail.MessagingException;

public interface EmailSenderService {

    @Async
    void sendEmail(EmailDtoText emailDtoText);

    void sendHtmlEmailFromTemplate(EmailDtoHtml emailDtoHtml) throws MessagingException;

    void sendRegistrationConfirmationEmail(UserToken token, String url) throws MessagingException;

    void sendPasswordRecoveryMail(UserToken token, String url) throws MessagingException;

}
