package com.example.ComputerScience.services.mappers;


import com.example.ComputerScience.models.dto.create.CreateDisciplineDto;
import com.example.ComputerScience.models.dto.create.CreateStudentDto;
import com.example.ComputerScience.models.dto.create.CreateTeacherDto;
import com.example.ComputerScience.models.dto.create.CreateUserDto;
import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.models.impl.Student;
import com.example.ComputerScience.models.impl.Teacher;
import com.example.ComputerScience.models.impl.User;

public interface ModelMapperService {
    Discipline toDiscipline(CreateDisciplineDto data, User taughtBy);

    Teacher toTeacher(CreateTeacherDto data);

    Student toStudent(CreateStudentDto data);

    User toUser(CreateUserDto data);
}
