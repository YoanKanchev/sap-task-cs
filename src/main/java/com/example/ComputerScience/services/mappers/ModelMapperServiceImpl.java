package com.example.ComputerScience.services.mappers;


import com.example.ComputerScience.models.dto.create.CreateDisciplineDto;
import com.example.ComputerScience.models.dto.create.CreateStudentDto;
import com.example.ComputerScience.models.dto.create.CreateTeacherDto;
import com.example.ComputerScience.models.dto.create.CreateUserDto;
import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.models.impl.Student;
import com.example.ComputerScience.models.impl.Teacher;
import com.example.ComputerScience.models.impl.User;
import org.springframework.stereotype.Service;

@Service
public class ModelMapperServiceImpl implements ModelMapperService {

    @Override
    public Discipline toDiscipline(CreateDisciplineDto data, User taughtBy) {
        Discipline discipline = new Discipline();
        discipline.setName(data.getName());
//        discipline.setTaughtBy(taughtBy);
        discipline.setCreditsGiven(data.getCreditsGiven());
        discipline.setCourse(data.getCourse());
        return discipline;
    }

    @Override
    public Teacher toTeacher(CreateTeacherDto data) {
        Teacher teacher = (Teacher) toUser(data);
        teacher.setTitle(data.getTitle());
        return teacher;
    }

    @Override
    public Student toStudent(CreateStudentDto data) {
        Student student = (Student) toUser(data);
        student.setCourse(data.getCourse());
        return student;
    }

    @Override
    public User toUser(CreateUserDto data) {
        User user = new User();
        user.setUsername(data.getUsername());
        user.setEmail(data.getEmail());
        user.setPassword(data.getPassword());
        user.setFirstName(data.getFirstName());
        user.setLastName(data.getLastName());
        return user;
    }
}
