package com.example.ComputerScience.services;

import com.example.ComputerScience.exceptions.BadRequestException;
import com.example.ComputerScience.exceptions.EntityNotFoundException;
import com.example.ComputerScience.models.dto.Dto;
import com.example.ComputerScience.models.dto.create.CreateStudentDto;
import com.example.ComputerScience.models.impl.Discipline;
import com.example.ComputerScience.models.impl.Student;
import com.example.ComputerScience.repositories.StudentRepository;
import com.example.ComputerScience.services.mappers.ModelMapperService;
import com.example.ComputerScience.services.validation.UserValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static com.example.ComputerScience.helpers.GlobalConstants.*;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final DisciplineService disciplineService;
    private final UserValidationService<Student> userValidationService;
    private final ModelMapperService mapperService;

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll(Sort.by(USERNAME).ascending());
    }

    @Override
    public Student getById(int id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(STUDENT + WITH_ID_NOT_FOUND_ERROR, id)));
    }

    @Override
    public Student getByUsername(String username) {
        return studentRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format(STUDENT + WITH_USERNAME_NOT_FOUND_ERROR, username)));
    }

    @Override
    public Student getByEmail(String email) {
        return studentRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(String.format(STUDENT + WITH_EMAIL_NOT_FOUND_ERROR, email)));
    }

    @Override
    public List<Student> getTopStudents() {
        return studentRepository.getTopStudents(Pageable.ofSize(MAX_SIZE_FOR_TOP_STUDENTS));
    }

    @Override
    public Student create(Dto data) {
        CreateStudentDto studentData = (CreateStudentDto) data;
        Student student = mapperService.toStudent(studentData);
        return studentRepository.save(student);
    }

    @Override
    public Student update(Dto data) {
        return null;
    }

    @Override
    public Student addDisciplineToStudent(int studentId, int disciplineId) {
        Student student = getById(studentId);
        Discipline discipline = disciplineService.getById(disciplineId);

        if (!Objects.equals(student.getCourse(), discipline.getCourse())) {
            throw new BadRequestException(BAD_REQUEST_MESSAGE + STUDENT_AND_DISCIPLINE_NOT_COMPATIBLE);
        }
        if (!student.addDiscipline(discipline)) {
            throw new BadRequestException(BAD_REQUEST_MESSAGE);
        }
        return studentRepository.save(student);
    }

    @Override
    public Student removeDisciplineFromStudent(int studentId, int disciplineId) {
        Student student = getById(studentId);
        Discipline discipline = disciplineService.getById(disciplineId);

        if (!student.removeDiscipline(discipline))
            throw new BadRequestException(BAD_REQUEST_MESSAGE);
        return studentRepository.save(student);
    }
}
