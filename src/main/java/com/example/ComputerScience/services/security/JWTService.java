package com.example.ComputerScience.services.security;

import org.springframework.security.core.userdetails.User;

public interface JWTService {
    void authenticateAccessToken(String accessToken);

    String refreshAccessToken(String requestURL, String refreshToken);

    String generateRefreshToken(String requestURL, User user);

    String generateAccessToken(String requestURL, User user);
}
