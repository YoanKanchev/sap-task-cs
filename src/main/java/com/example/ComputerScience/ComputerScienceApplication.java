package com.example.ComputerScience;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScans({@ComponentScan("com.example.ComputerScience.controllers")})
@EnableJpaRepositories("com.example.ComputerScience.repositories")
@EntityScan("com.example.ComputerScience.models.impl")
@EnableWebMvc
public class ComputerScienceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComputerScienceApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }
}
