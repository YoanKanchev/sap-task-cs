package com.example.ComputerScience.helpers.filter;//package com.example.com.example.demo.helpers.filter;
//
//
//import org.springframework.data.domain.Sort;
//import org.springframework.stereotype.Component;
//
//
//@Component
//public class FilterBuilder {
//
//    public FilterSpecification<AppService> createSpecification(AppServiceFilterParameterDto appServiceFilterParameterDto) {
//        String type = appServiceFilterParameterDto.getType();
//        String creatorName = appServiceFilterParameterDto.getCreatorName();
//        Integer rating = appServiceFilterParameterDto.getRating();
//        return buildAppServiceSpecification(type, creatorName, rating);
//    }
//
//    public FilterSpecification<User> createSpecification(UserFilterParametersDto userFilterParametersDto) {
//        return buildUserSpecification(userFilterParametersDto.getUsernameOrMail(), userFilterParametersDto.getRole());
//    }
//
//    public <T extends FilterParameters> Sort getSortOrders(T filterParameter) {
//        return (filterParameter.isSorted()) ?
//                Sort.by(getSortDirection(filterParameter.getIsOrderAsc()), filterParameter.getSortBy()) :
//                Sort.by(getSortDirection(filterParameter.getIsOrderAsc()));
//    }
//
//    public Sort.Direction getSortDirection(Boolean isOrderAsc) {
//        return isOrderAsc ? Sort.Direction.ASC : Sort.Direction.DESC;
//    }
//
//    private FilterSpecification<AppService> buildAppServiceSpecification(String type, String creatorName, Integer rating) {
//        FilterSpecification<AppService> appFilterSpecification = new AppServiceFilterSpecification();
//        if (type != null)
//            appFilterSpecification.add(new SearchCriteria(type, AppServiceSearchOperation.TYPE));
//        if (creatorName != null)
//            appFilterSpecification.add(new SearchCriteria(creatorName, AppServiceSearchOperation.CREATOR));
//        if (rating != null)
//            appFilterSpecification.add(new SearchCriteria(rating, AppServiceSearchOperation.GREATER_OR_EQUAL_TO_RATING));
//        return appFilterSpecification;
//    }
//
//    private FilterSpecification<User> buildUserSpecification(String usernameOrMail, String role) {
//        FilterSpecification<User> userFilterSpecificationImpl = new UserFilterSpecificationImpl();
//
//        if (usernameOrMail != null)
//            userFilterSpecificationImpl.add(new SearchCriteria(usernameOrMail, USERNAMEOREMAIL));
//        if (role != null)
//            userFilterSpecificationImpl.add(new SearchCriteria(role, ROLE));
//
//        return userFilterSpecificationImpl;
//    }
//}
