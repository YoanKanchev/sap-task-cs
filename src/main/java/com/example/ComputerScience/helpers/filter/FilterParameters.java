package com.example.ComputerScience.helpers.filter;

public interface FilterParameters {
    Boolean isSorted();

    Boolean getIsOrderAsc();

    String getSortBy();
}
