package com.example.ComputerScience.helpers;

public class GlobalConstants {


    //MODELS
    public static final String TEACHER_INFO = "teacherInfo";
    public static final String STUDENT_INFO = "studentInfo";
    public static final String DISCIPLINE_INFO = "disciplineInfo";
    public static final String ALL_TEACHERS = "allTeachers";
    public static final String ALL_STUDENTS = "allStudents";
    public static final String ALL_DISCIPLINES = "allDisciplines";
    public static final String CREATE_TEACHER_DTO = "createTeacherDto";
    public static final String CREATE_STUDENT_DTO = "createStudentDto";
    public static final String CREATE_DISCIPLINE_DTO = "createDisciplineDto";
    public static final String AVAILABLE_DISCIPLINES = "availableDisciplines";
    public static final String TOP_TEACHERS = "topTeachers";
    public static final String TOP_DISCIPLINES = "topDisciplines";

    //Messages
    public static final String NOT_FOUND_ERROR = " not found!";
    public static final String BAD_REQUEST_MESSAGE = "Bad request!";
    public static final String STUDENT_AND_DISCIPLINE_NOT_COMPATIBLE = "The student and the discipline must be from one course!";
    public static final String ADDED_SUCCESSFULLY = " %s %s added successfully";
    public static final String ADDED_SUCCESSFULLY_SHORT = " %s added successfully";
    public static final String WITH_ID_NOT_FOUND_ERROR = " with id %d not found!";
    public static final String WITH_USERNAME_NOT_FOUND_ERROR = " with username %s not found!";
    public static final String WITH_EMAIL_NOT_FOUND_ERROR = " with email %s not found!";
    public static final String UNAUTHORISED_ERROR = "You are unauthorised!!";
    public static final String WRONG_CONFIRMATION_PASSWORD = "Wrong confirmation password!";
    public static final String WITH_NAME_EXISTS_ERROR = " with name:%s already exists";
    public static final String WITH_EMAIL_EXISTS_ERROR = " with email:%s already exists";

    //ENTITY
    public static final String USER = "User";
    public static final String TEACHER = "Teacher";
    public static final String STUDENT = "Student";
    public static final String DISCIPLINE = "Discipline";
    public static final String TOKEN_ENTITY = "Token";

    // HQL
    public static final int MAX_SIZE_FOR_TOP_DISCIPLINES = 3;
    public static final int MAX_SIZE_FOR_TOP_TEACHERS = 3;
    public static final int MAX_SIZE_FOR_TOP_STUDENTS = 3;
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String USERNAME = "username";

    //EMAIL
    public static final String PATH_FOR_PASSWORD_RECOVERY_HANDLER = "/sign-up/recover-password/verify?token=";
    public static final String PATH_FOR_REGISTRATION_CONFIRMATION = "/sign-up/verify?token=";
    public static final String SUBJECT_FOR_REGISTRATION_CONFIRMATION = "Computer Science New Account Confirmation";
    public static final String SUBJECT_FOR_PASSWORD_RECOVERY = "Computer Science Password Recovery";
    public static final String CONFIRMATION_EMAIL_SEND = "An confirmation e-mail has been send to your e-mail address, please follow the link before continuing.";
    public static final String YOUR_LINK_HAS_EXPIRED = "Your link has expired...";
    public static final String TEMPLATE_FOR_PASSWORD_RECOVERY = "password-reset-email-template.html";
    public static final String TEMPLATE_FOR_REGISTRATION_CONFIRMATION = "registration-confirmation-email-template.html";


    //JTW
    public static final String JWT_TOKEN_PREFIX = "Bearer ";
    public static final String JWT_SECRET = "secret%!(";
    public static final long DAY_IN_MILLISECONDS = 86_400_000;
    public static final long HOUR_IN_MILLISECONDS = 3_600_000;

}
