package com.example.ComputerScience.helpers.enums;

public enum TokenType implements SimpleEnum {
    REGISTRATION_CONFIRMATION,
    RECOVER_PASSWORD;

    @Override
    public String toString() {
        return this.name();
    }
}
