package com.example.ComputerScience.helpers.enums;

public interface SimpleEnum {
    String toString();
}
