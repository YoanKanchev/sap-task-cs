package com.example.ComputerScience.helpers.enums;

public enum Role {
    ROLE_USER,
    ROLE_STUDENT,
    ROLE_TEACHER,
    ROLE_ADMIN
}
