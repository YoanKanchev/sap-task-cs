package com.example.ComputerScience.helpers.anotations.anotationValidators;


import com.example.ComputerScience.helpers.anotations.ValidUserRole;
import com.example.ComputerScience.helpers.enums.Role;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class UserRoleValidator implements ConstraintValidator<ValidUserRole, String > {
    @Override
    public void initialize(ValidUserRole constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(Role.values()).anyMatch(x->x.toString().equals(value));
    }
}
