package com.example.ComputerScience.config.security;

import com.example.ComputerScience.config.security.filter.AuthenticationFilter;
import com.example.ComputerScience.config.security.filter.AuthorisationFilter;
import com.example.ComputerScience.controllers.error.ErrorController;
import com.example.ComputerScience.services.security.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.example.ComputerScience.helpers.enums.Role.*;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JWTService jwtService;
    private final ErrorController errorController;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        AuthenticationFilter authenticationFilter = new AuthenticationFilter(authenticationManagerBean(), jwtService);
        authenticationFilter.setFilterProcessesUrl("/api/login");
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers("/assets/**", "/css/**", "/fonts/**", "/images/**", "/js/**", "/**").permitAll();
        http.authorizeRequests().antMatchers("/api/login/**", "/api/security/refreshToken/**","api/swagger-ui/**").permitAll();
        http.authorizeRequests().antMatchers(GET, "/api/students/**", "/api/teachers/**").hasAnyAuthority(ROLE_STUDENT.toString(), ROLE_TEACHER.toString(), ROLE_USER.toString());
//        http.authorizeRequests().antMatchers(POST, "/api/students/{id}/assignDiscipline").hasAnyAuthority(ROLE_ADMIN.toString());
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(authenticationFilter);
        http.addFilterBefore(new AuthorisationFilter(jwtService, errorController), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
