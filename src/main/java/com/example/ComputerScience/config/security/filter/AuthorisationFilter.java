package com.example.ComputerScience.config.security.filter;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.ComputerScience.controllers.error.ErrorController;
import com.example.ComputerScience.services.security.JWTService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.ComputerScience.helpers.GlobalConstants.JWT_TOKEN_PREFIX;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
public class AuthorisationFilter extends OncePerRequestFilter {
    private final JWTService jwtService;
    private final ErrorController errorController;

    public AuthorisationFilter(JWTService jwtService, ErrorController errorController) {
        this.jwtService = jwtService;
        this.errorController = errorController;

    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().equals("/api/login") || request.getServletPath().equals("/api/security/refreshToken")) {
            filterChain.doFilter(request, response);
        } else {
            String authorisation = request.getHeader(AUTHORIZATION);

            if (Strings.isNotEmpty(authorisation) && authorisation.startsWith(JWT_TOKEN_PREFIX)) {
                try {
                    String token = authorisation.replace(JWT_TOKEN_PREFIX, "");
                    jwtService.authenticateAccessToken(token);
                    filterChain.doFilter(request, response);
                } catch (TokenExpiredException e) {
                    errorController.handleTokenExpiredConflict(e,response);
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }
}
