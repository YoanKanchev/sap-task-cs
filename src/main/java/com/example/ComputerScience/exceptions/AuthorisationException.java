package com.example.ComputerScience.exceptions;

public class AuthorisationException extends RuntimeException {

    public AuthorisationException(String message) {
        super(message);
    }
}
