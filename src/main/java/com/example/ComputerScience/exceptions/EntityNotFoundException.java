package com.example.ComputerScience.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException (String message) {
        super(message);
    }
}
