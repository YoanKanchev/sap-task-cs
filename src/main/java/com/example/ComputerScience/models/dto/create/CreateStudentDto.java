package com.example.ComputerScience.models.dto.create;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Getter
@Setter
public class CreateStudentDto extends CreateUserDto {

    @Min(value = 1, message = "Course must be from I to V")
    @Max(value = 5, message = "Course must be from I to V")
    private Integer course;

}
