package com.example.ComputerScience.models.dto.create;

import com.example.ComputerScience.helpers.anotations.PasswordValueMatch;
import com.example.ComputerScience.helpers.anotations.ValidPassword;
import com.example.ComputerScience.models.dto.Dto;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "password",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
@Data
public class CreateUserDto implements Dto {

    @Size(min = 2, max = 20, message = "Username must be between 2 and 20 characters!")
    private String username;
    @Email
    private String email;
    @ValidPassword
    private String password;
    private String confirmPassword;
    @Size(min = 2, max = 20, message = "First name must be between 2 and 20 characters!")
    private String firstName;
    @Size(min = 2, max = 20, message = "Last name must be between 2 and 20 characters!")
    private String lastName;
}
