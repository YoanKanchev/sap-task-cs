package com.example.ComputerScience.models.dto.update;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdateTeacherDto extends UpdateUserDto {

    @Size(min = 2, max = 50, message = "Title must be between 2 and 50 characters!")
    private String title;
}
