package com.example.ComputerScience.models.dto.update;

import com.example.ComputerScience.models.dto.Dto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UpdateDisciplineDto implements Dto {

    @Size(min = 2, max = 30, message = "Discipline name must be between 2 and 30 characters!")
    private String name;

    private Integer taughtById;

    @Min(value = 1, message = "Discipline credits must be between 1 and 100!")
    @Max(value = 100, message = "Discipline credits must be between 1 and 100!")
    private Integer creditsGiven;

    @Min(value = 1, message = "Course must be from 1 to 5!")
    @Max(value = 5, message = "Course must be from 1 to 5!")
    private Integer course;
}
