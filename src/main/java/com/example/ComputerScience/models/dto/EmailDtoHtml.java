package com.example.ComputerScience.models.dto;

import java.util.Map;

public record EmailDtoHtml(String to, String subject, String template, Map<String, Object> templateModel) {
}
