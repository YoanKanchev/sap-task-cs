package com.example.ComputerScience.models.dto;

import com.example.ComputerScience.helpers.anotations.PasswordValueMatch;
import com.example.ComputerScience.helpers.anotations.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@PasswordValueMatch.List({
        @PasswordValueMatch(
                field = "newPassword",
                fieldMatch = "confirmPassword",
                message = "Passwords do not match!"
        )
})
public class ChangePasswordDto {
    @NotEmpty
    private String oldPassword;
    @ValidPassword
    private String newPassword;
    private String confirmPassword;
}
