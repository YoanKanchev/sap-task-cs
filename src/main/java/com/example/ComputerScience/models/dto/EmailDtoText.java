package com.example.ComputerScience.models.dto;

public record EmailDtoText(String to, String subject, String message) {

}
