package com.example.ComputerScience.models.impl;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "teachers")
public class Teacher extends User {

    @Column(name = "title")
    private String title;

    @ManyToMany(mappedBy = "taughtBy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Discipline> disciplinesToTeach;
}
