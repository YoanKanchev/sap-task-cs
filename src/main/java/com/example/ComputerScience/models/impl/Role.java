package com.example.ComputerScience.models.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@Table(name = "roles")
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected Integer id;

    @Column(name = "role_name")
    private String roleName;

    @Override
    @JsonIgnore
    public String getAuthority() {
        return roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Role role = (Role) o;
        return Objects.equals(id, role.id) || Objects.equals(roleName, role.roleName);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = id != null ? result + id.hashCode() : result;
        result = roleName != null ? result + roleName.hashCode() : result;
        return result;
    }
}
