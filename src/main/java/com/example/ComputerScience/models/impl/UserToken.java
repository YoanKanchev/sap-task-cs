package com.example.ComputerScience.models.impl;

import com.example.ComputerScience.helpers.enums.TokenType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "user_tokens")
@RequiredArgsConstructor
public class UserToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "value")
    private String value;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TokenType type;


    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "expiration_date")
    private Date expirationDate;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;


    public UserToken(String value, TokenType type, User user, Integer expirationTimeInHours) {
        this.user = user;
        this.type = type;
        setExpirationDate(expirationTimeInHours);
        this.value = value;
    }

    private void setExpirationDate(int expirationTimeInHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationDate);
        calendar.add(Calendar.HOUR, expirationTimeInHours);
        this.expirationDate = calendar.getTime();
    }

    public boolean isExpired() {
        long expirationDateMilliseconds = expirationDate.getTime();
        long currentDateMilliseconds = new Date().getTime();
        return currentDateMilliseconds > expirationDateMilliseconds;
    }

}
