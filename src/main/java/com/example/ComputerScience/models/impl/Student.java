package com.example.ComputerScience.models.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "students")
public class Student extends User{

    @Column(name = "course")
    private Integer course;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "students_disciplines", joinColumns = @JoinColumn(name = "student_id"), inverseJoinColumns = @JoinColumn(name = "discipline_id"))
    private Set<Discipline> disciplinesToStudy;

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getCourse() {
        return course;
    }

    public Integer getCredits() {
        int credits = 0;
        for (Discipline discipline : disciplinesToStudy) {
            credits += discipline.getCreditsGiven();
        }
        return credits;
    }

    @JsonIgnore
    public ArrayList<Discipline> getDisciplinesList() {
        return new ArrayList<>(disciplinesToStudy);
    }

    public boolean addDiscipline(Discipline discipline) {
        return disciplinesToStudy.add(discipline);
    }

    public boolean removeDiscipline(Discipline discipline) {
        return disciplinesToStudy.remove(discipline);
    }
}
