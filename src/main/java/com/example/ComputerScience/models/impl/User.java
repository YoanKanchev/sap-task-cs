package com.example.ComputerScience.models.impl;

import com.example.ComputerScience.models.contracts.Searchable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Searchable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @JsonIgnore
    @Column(name = "password")
    private String password;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "is_active")
    private boolean isActive;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public boolean hasRole(String role) {
        return roles.stream().anyMatch(xRole -> xRole.getRoleName().equals(role));
    }

    public void addRoles(Role... role) {
        roles.addAll(Arrays.asList(role));
    }

    public void removeRoles(Role... role) {
        Arrays.asList(role).forEach(roles::remove);
    }

    @JsonIgnore
    public String getFullName() {
        return firstName + " " + lastName;
    }
}
