package com.example.ComputerScience.repositories;

import com.example.ComputerScience.models.impl.UserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<UserToken, Integer> {

		Optional<UserToken> findByValue(String tokenValue);
}
