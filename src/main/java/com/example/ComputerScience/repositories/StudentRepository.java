package com.example.ComputerScience.repositories;

import com.example.ComputerScience.models.impl.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends UserRepository<Student> {
    @Query("FROM Student s " +
            "LEFT JOIN s.disciplinesToStudy dts " +
            "GROUP BY s.id " +
            "ORDER BY SUM(dts.creditsGiven) desc")
    List<Student> getTopStudents(Pageable numberOfResults);
}
