package com.example.ComputerScience.repositories;

import com.example.ComputerScience.models.impl.Teacher;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends UserRepository<Teacher> {
    @Query("FROM Teacher t " +
            "join t.disciplinesToTeach dtt " +
            "order by count(dtt.students) desc")
    List<Teacher> getTopTeachers(Pageable numberOfResults);
}
