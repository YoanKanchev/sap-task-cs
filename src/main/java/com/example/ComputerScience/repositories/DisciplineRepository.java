package com.example.ComputerScience.repositories;

import com.example.ComputerScience.models.impl.Discipline;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplineRepository extends JpaRepository<Discipline, Integer>, JpaSpecificationExecutor<Discipline> {

    @Query(value = "from Discipline d order by SIZE(d.students) DESC")
    List<Discipline> getTopDisciplines(Pageable pageable);

    List<Discipline> findAllByCourse(Integer course);

}