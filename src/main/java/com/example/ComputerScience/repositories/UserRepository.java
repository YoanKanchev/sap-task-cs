package com.example.ComputerScience.repositories;

import com.example.ComputerScience.models.impl.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository<T extends User> extends JpaRepository<T, Integer>, JpaSpecificationExecutor<T> {

    Optional<T> findByUsername(String username);

    Optional<T> findByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsByUsername(String email);

}
